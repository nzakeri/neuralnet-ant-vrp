'''
Created on Sep 16, 2017

@author: negar
'''

import tensorflow as tf

sess = tf.Session()

writer = tf.summary.FileWriter("../../tflog/outout")
i_tensor = tf.Variable(0)
summ = tf.summary.scalar("my i", i_tensor)
for i in range(100):
    summ_res = summ.eval({i_tensor:i},session=sess)
    summ_writer = writer.add_summary(summ_res, i)
    print (i)
