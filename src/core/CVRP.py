'''
Created on Sep 14, 2017

@author: negar
'''
import math
import numpy as np
class CVRP(object):
    '''
    classdocs
    '''


    def __init__(self, lines):
        '''
        Constructor
        '''
        section = 0
        self.locs = []
        for line in lines:
            if line.find("DIMENSION") >= 0:
                line = line.strip(' \t\n\r')
                self.dimension = int(line.split()[2])
                
            if line.find("CAPACITY") >= 0:
                line = line.strip(' \t\n\r')
                self.vehCap = int(line.split()[2])
             
            if line.find("NODE_COORD_SECTION") >= 0:
                section = 1
                continue
            
            if line.find("DEMAND_SECTION") >= 0:
                section = 2
                continue
            if line.find("DEPOT_SECTION") >= 0:
                break
            if section == 1:
                line = line.strip(' \t\n\r')
                x = int(line.split()[1])
                y = int(line.split()[2])
                self.locs.append(Location(x, y))
            if section == 2:                
                line = line.strip(' \t\n\r')
                nodeId = int(line.split()[0])-1 #// id count starts from zero not one. we are in 2017.
                demand = int(line.split()[1])
                self.locs[nodeId].setDemand(demand)
                
    
    def getMaxOptionIdx(self):
        return self.dimension

    def getCurOptions(self, way):                    
        if not way:
            return [0]
        retVal = []
        curCapacityUsed = 0;
        i = len(way)-1
        while i >= 0 and way[i] != 0:
            curCapacityUsed += self.locs[way[i]].demand
            i -= 1
            
        for i in range(len(self.locs)):     
            if i not in way and self.locs[i].demand+curCapacityUsed < self.vehCap:
                retVal.append(i)
                
        if not way[len(way)-1] == 0:
            retVal.append(0)
        
        return retVal
        

    def getCost(self, way):
        cost = 0
        for i in range(len(way)-1):
            disX = (self.locs[way[i+1]].x) - (self.locs[way[i]].x)
            disY = (self.locs[way[i+1]].y) - (self.locs[way[i]].y)
            sumTotal = math.pow(disX, 2) + math.pow(disY, 2)
            cost += math.sqrt(sumTotal)
        return cost
     
    def isCompleteFeasible(self, way):
        capacity = 0
        numVisit = [0]*len(self.locs)
        for loc in way:
            numVisit[loc] += 1;
        for i in range(1, len(numVisit)):
            if not numVisit[i] == 1:
                return False
        for i in range(len(way)):
            if not way[i] == 0:
                capacity += self.locs[way[i]].demand
            else:
                if capacity > self.vehCap:
                    return False
                else:
                    capacity = 0
                    
        if (capacity > self.vehCap) or (not way[len(way)-1] == 0):
            return False
        return True    
    
    def getlocations(self):
        return [(l.x,l.y) for l in self.locs]
    
    
    def getHeuristic(self):
        infinity = np.float32(1.0) 
        locArr = self.getlocations()
        disMat = np.full([len(locArr), len(locArr)], 0.0) 
        self.heuristic = np.full(disMat.shape, 0.0)  
        for i in range(len(locArr)):
            for j in range(len(locArr)):                
                xDiff = locArr[i][0] - locArr[j][0];
                yDiff = locArr[i][1] - locArr[j][1];
                              
                disMat[i,j] = math.fabs(math.sqrt((xDiff * xDiff) + (yDiff * yDiff)))
                if disMat[i,j] == 0.0:
                    self.heuristic[i,j] = infinity
                else:                
                    self.heuristic[i,j] = 1.0 / disMat[i,j]  
        
        return self.heuristic

class Location():
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def setDemand(self, demand):
        self.demand = demand
        
            

    
