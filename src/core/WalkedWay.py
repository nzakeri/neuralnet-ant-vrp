'''
Created on Sep 8, 2017

@author: negar
'''

class WalkedWay(object):
    
    def __init__(self, way, cost, feasibility, deltaPheromone):
        self.way = way
        self.cost = cost
        self.feasibility = feasibility
        self.deltaPheromone = deltaPheromone
        pass

    def __str__(self, *args, **kwargs):
        return str(self.way)

    def __dir__(self, *args, **kwargs):
        return str(self.way)