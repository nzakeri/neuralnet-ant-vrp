'''
Created on Sep 14, 2017

@author: negar
'''
import unittest
from core.CVRP import CVRP

class CVRPTest(unittest.TestCase):


    def testInit(self):
        lines = [
                "NAME : A-n32-k5",
                "COMMENT : (Augerat et al, Min no of trucks: 5, Optimal value: 784)",
                "TYPE  : CVRP",
                "DIMENSION  :  4",
                "EDGE_WEIGHT_TYPE  : EUC_2D", 
                "CAPACITY  :   100",
                "NODE_COORD_SECTION",
                "  1 0 0",
                "2  5 0 ",
                "3 0 5 ",
                " 4 5 5",
                "DEMAND_SECTION",
                " 1 0", 
                " 2 40",
                "3 40 ", 
                "4  40 ", 
                "DEPOT_SECTION",
                "1",
                "-1",
                "EOF" 
                ]
           
        p = CVRP (lines)
        self.assertEqual(4, p.getMaxOptionIdx())
        way = []
        options = p.getCurOptions(way)
        print("This is the Options: ", options)        
        self.assertEqual(set([0]), set(options))
        
        
        way.append(0)
        options2 = p.getCurOptions(way)
        print("This is the Options2: ", options2) 
        self.assertEqual(set([1,2,3]), set(options2))
        
        way.append(1)
        options3 = p.getCurOptions(way)
        print("This is the Options3: ", options3) 
        self.assertEqual(set([0,2,3]), set(options3))
        
        way.append(2)
        way.append(3)
        options4 = p.getCurOptions(way)
        print("This is the Options4: ", options4) 
        self.assertEqual(set([0]), set(options4))

        way.append(0)
        options5 = p.getCurOptions(way)
        print("This is the Options5: ", options5) 
        print(way)
        self.assertEqual(set([]), set(options5))

        costWay = p.getCost(way)
        self.assertAlmostEqual(24.14, costWay, places = 2)
        feasibility = p.isCompleteFeasible(way)
        self.assertFalse(feasibility)
        
        way2 = [0,1,3,0,2,0]
        self.assertTrue(p.isCompleteFeasible(way2))
        
        way3 = [0,1,3,0,2]
        self.assertFalse(p.isCompleteFeasible(way3))
        
        way3 = [0,1,3,0,2,0,1]
        self.assertFalse(p.isCompleteFeasible(way3))
        
        way3 = [0,1,3,0,0,2,0]
        self.assertTrue(p.isCompleteFeasible(way3))

 



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    
    