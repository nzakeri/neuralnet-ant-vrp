'''
Created on Sep 25, 2017

@author: negar
'''
import unittest
import numpy as np
from core.AntSolver import AntSolver
from core.WalkedWay import WalkedWay
import numpy.testing as npt
from core.SimpleAntBrain import SimpleAntBrain
import math
import tensorflow as tf
from numpy import dtype


class Test(unittest.TestCase):


    def testName(self):
        pass
    def testTensorProb(self):
        sess = tf.Session()
        with sess.as_default():
            nNodes = 4
            nAnts = 3
            alpha = 1
            beta =2
            heuristic = np.full((4,4),1.0)       
            initphere= 2.0            
            simplBrain = SimpleAntBrain(nNodes, alpha, beta, 0.0, heuristic, initphere)              
            simplBrain.initialize(3)
            paths = [
                    WalkedWay([0,1],0,0,0),
                    WalkedWay([0,3],0,0,0),
                    WalkedWay([0,2],0,0,0),
                    ]
            wayArr = [[0,1], [0,3], [0,2]]
        #    prob = simplBrain.computeProbability(paths) 
            prob = simplBrain.computeProbability(wayArr)     
            for i in range(nAnts):
                for j in range(nNodes):
                    exp_val = 0
                #    x=paths[i].way[-1]
                    x = wayArr[i][-1]
                    
                    y=j
                    exp_val =  math.pow(initphere, simplBrain.alpha) * math.pow(simplBrain.heuristic[x, y], simplBrain.beta)
                    # print(exp_val)
                    self.assertAlmostEqual(prob[i][j],exp_val)
    def testComputePheromoneDeltaMat(self):
        paths = [
                WalkedWay([0,1,2],0,0,0.1),
                WalkedWay([0,3,1],0,0,0.1),
                WalkedWay([0,2,1],0,0,0.1),
                ]
        
        simpleBrain = SimpleAntBrain(4, 3, 0, 0, 0, 0, 0) 
        res = simpleBrain.computePheromoneDeltaMat(paths)             
        
        np.testing.assert_array_equal(
            
            np.array([
                [0.0, 0.1, 0.1, 0.1],
                [0.0, 0.0, 0.1, 0.0],
                [0.0, 0.1, 0.0, 0.0],
                [0.0, 0.1, 0.0, 0.0],                
            ],dtype=np.float32),
            res
            )         
            
    def testUpdatePheromone(self):
        
        fakeDeltaMat = np.array([
                [0.0, 0.1, 0.1, 0.1],
                [0.0, 0.0, 0.1, 0.0],
                [0.0, 0.1, 0.0, 0.0],
                [0.0, 0.1, 0.0, 0.0],                
            ],dtype=np.float32)
        
        sess = tf.Session()
        with sess.as_default():
            rho = 0.2
            simpleBrain = SimpleAntBrain(4, 3, 0, 0, rho, fakeDeltaMat, 0.0) 
            simpleBrain.initialize()
            pherMat = simpleBrain.pherVariable.eval()
            
            def mockComputePheromone(mockPath):
                x = np.array(fakeDeltaMat)
                return x
            simpleBrain.computePheromoneDeltaMat = mockComputePheromone             

            
            mockWalkedWay = [];
            simpleBrain.updatePheromoneMat(mockWalkedWay)

            pherMat = simpleBrain.pherVariable.eval()
            np.testing.assert_array_equal(fakeDeltaMat,
            pherMat
            )
            
            simpleBrain.updatePheromoneMat(mockWalkedWay)
            pherMat = simpleBrain.pherVariable.eval()
            
            np.testing.assert_array_almost_equal(
            fakeDeltaMat*1.8,
            pherMat,8,
            )

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()