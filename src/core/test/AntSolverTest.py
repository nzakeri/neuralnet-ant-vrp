'''
Created on Sep 6, 2017

@author: negar
'''
import unittest
import numpy as np
from core.AntSolver import AntSolver
from core.WalkedWay import WalkedWay
import numpy.testing as npt



class RandomMock():
    def __init__(self, num):
        self.num = num
    
    def random(self):
        return self.num


class Test(unittest.TestCase):


    def testName(self):
#         self.assertEqual(2, 3, 'ahhhh')
        pass
    
    def testGenerateRandNode(self):    
        mat = np.array([5.0,5.0])
        rand = RandomMock( 0.2 )
        self.assertEquals(0,AntSolver.generateRandNode(mat,rand))

        rand = RandomMock( 1.0 )
        self.assertEquals(1,AntSolver.generateRandNode(mat,rand))
        
        
    def testcomputePheromoneDelta(self):  
        
        self.assertAlmostEqual(
                    1/1005.0,
                    AntSolver.computePheromoneDelta( 5.0 ,False,1.0,10000.0,1000.0),
                    places=3,
                    )
        
    def testcomputePheromoneDelta2(self):
        self.assertAlmostEqual(
                    10000.0,
                    AntSolver.computePheromoneDelta( 0.0 ,True, 1.0, 10000.0, 1000.0),
                    places=3,
                    )

    def testPheromoneUpdate(self):
        nNode = 10
        pherMat = np.full((nNode,nNode),1.0, dtype=np.float32)
        print (pherMat)
        tmpWay = [0,2,5,4,0]
        way = WalkedWay(tmpWay, 10.0, True , 1.0);
        AntSolver.pheromoneUpdate_impel (pherMat, way)
        for i in range(nNode):
            for j in range(nNode):
                expectedVal = 1.0
                for k in range(len(tmpWay)-1):
                    if i == tmpWay[k] and j == tmpWay[k+1]:
                        expectedVal = 2.0
                
                self.assertAlmostEqual(expectedVal, pherMat[i,j], places = 5)  
              
    def testOptionProbability(self):
        solver = lambda: None        
        setattr(solver, 'pheromones', np.full((3,3),0.5))
        setattr(solver, 'heuristic', np.full((3,3),0.2))
        setattr(solver, 'alpha', 2)
        setattr(solver, 'beta', 3)
        prob = AntSolver.optionProbability(solver, 1, 2)
        self.assertAlmostEqual(0.5*0.5*0.2*0.2*0.2, prob)
        
    def testInitDistHeuristic(self):
        locArr = [(0,0),(3,4)]
        #expVal = np.full((2,2),1)  
        expVal = np.array([[1.0, 0.2],[ 0.2, 1.0]])
             
        print(expVal)
        solver = lambda: None
        setattr(solver, 'infinity', 1) 
        AntSolver.initDistHeuristic(solver, locArr)
        print(solver.heuristic)
        npt.assert_allclose(expVal, solver.heuristic)
        
    def testdecayPheromone(self):
        solver = lambda: None        
        setattr(solver, 'pheromones', np.full((3,3),5.0))
        setattr(solver, 'rho', 0.8)
        expVal = np.full((3,3),1.0)
        AntSolver.decayPheromone(solver)        
        npt.assert_allclose(expVal, solver.pheromones)
        
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    