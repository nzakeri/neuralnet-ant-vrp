'''
Created on Sep 5, 2017

@author: negar
'''
import numpy
import math
import tensorflow as tf
from core.WalkedWay import WalkedWay


class AntSolver(object):
    '''
    classdocs
    '''


    def __init__(self, problemIns, alpha, beta, rho, Q, nAnts, initPheromone, nIterations, seed, log_location, log_interval, penalty):
        '''
        Constructor
        '''
        self.problem = problemIns
        self.alpha = numpy.float32(alpha)
        self.beta = numpy.float32(beta)
        self.rho = numpy.float32(rho)
        self.Q = numpy.float32(Q)
        self.nAnts = nAnts
        self.initPheromone = numpy.float32(initPheromone)
        self.nIterations = nIterations
        self.rand = numpy.random.RandomState(seed)
        
        self.posInf = 100000.0
        self.penalty = penalty 
        self.infinity = numpy.float32(1)       
        
        self.log_interval = log_interval
        
        if (self.log_interval > 0):
        
            self.writer = tf.summary.FileWriter(log_location)        
            self.cost_tensor = tf.Variable([1.0]*self.nAnts) # in ACO is the path's length
            self.feasibility_tensor = tf.Variable([1.0]*self.nAnts)
            self.delta_pher_tensor = tf.Variable([1.0]*self.nAnts)
            nNodes = self.problem.getMaxOptionIdx();
            self.pher_tensor = tf.Variable(numpy.zeros([nNodes,nNodes]))
            self.best_delta_pher_tensor = tf.Variable(0.0)
            self.best_cost_tensor = tf.Variable(0.0)
            
            tf.summary.histogram("cost", self.cost_tensor)
            tf.summary.histogram("feasibility", self.feasibility_tensor)
            tf.summary.histogram("delta_pher", self.delta_pher_tensor)
            tf.summary.histogram("pheromones", self.pher_tensor)
            
            tf.summary.scalar('cost_mean', tf.reduce_mean(self.cost_tensor))
            tf.summary.scalar('feas_mean', tf.reduce_mean(self.feasibility_tensor))
            tf.summary.scalar('delta_pher_mean', tf.reduce_mean(self.delta_pher_tensor))
            tf.summary.scalar('best_delta_pher', self.best_delta_pher_tensor)
            tf.summary.scalar('best_cost', self.best_cost_tensor)
            
            
            self.all_summ = tf.summary.merge_all()
               
        
        
    def run(self):
        self.initializePheromone()
        self.initDistHeuristic(self.problem.getlocations())
        #nIterations = total number of iterations; number of groups
        best = WalkedWay([], 0.0, 0.0, 0.0)
       # probsize = self.problem.getMaxOptionIdx()
       # smartPherUpdate = SimpleAntBrain(probsize, self.nAnts, self.alpha, self.beta, self.heuristic, self.initPheromone)
       # smartPherUpdate.initializePheromone()                                  
        for i in range(self.nIterations):
            
            paths = []
            
            for _ in range(self.nAnts):
                paths.append(self.constructPathForAnt())
            
            self.decayPheromone()
            
            for path in paths:
                self.pheromoneUpdate(path)                
                if path.deltaPheromone > best.deltaPheromone:
                    best = path
            
            print (best.way)
            
            if (i % self.log_interval == 0 and self.log_interval >= 0):
                
                costs = [p.cost for p in paths]
                feases = [p.feasibility for p in paths]
                delta_pher = [p.deltaPheromone for p in paths]
                
                summ_res = self.all_summ.eval({ self.cost_tensor: costs,
                                                self.feasibility_tensor: feases,
                                                self.delta_pher_tensor: delta_pher,
                                                self.pher_tensor: self.pheromones,
                                                self.best_delta_pher_tensor: best.deltaPheromone,
                                                self.best_cost_tensor: best.cost,
                                            })
                self.writer.add_summary(summ_res, i)
                self.writer.flush();
                print('.', end='', flush=True)
                
        return best


  
    def initializePheromone(self):
        probsize = self.problem.getMaxOptionIdx()
        self.pheromones = numpy.full([probsize, probsize], self.initPheromone)
        
    def initDistHeuristic(self, locArr):
        disMat = numpy.full([len(locArr), len(locArr)], 0.0) 
        self.heuristic = numpy.full(disMat.shape, 0.0)  
        for i in range(len(locArr)):
            for j in range(len(locArr)):                
                xDiff = locArr[i][0] - locArr[j][0];
                yDiff = locArr[i][1] - locArr[j][1];
                              
                disMat[i,j] = math.fabs(math.sqrt((xDiff * xDiff) + (yDiff * yDiff)))
                if disMat[i,j] == 0.0:
                    self.heuristic[i,j] = self.infinity
                else:                
                    self.heuristic[i,j] = 1.0 / disMat[i,j]                   
                        
        
        
    @staticmethod
    def pheromoneUpdate_impel(pheromones, walkedWay):
        deltaPheromone = walkedWay.deltaPheromone; 
        for i in range(len(walkedWay.way)-1):
            a = walkedWay.way[i]
            b = walkedWay.way[i+1]
            pheromones[a,b] += deltaPheromone
  
        

    def pheromoneUpdate(self, walkedWay):
        AntSolver.pheromoneUpdate_impel(self.pheromones, walkedWay)    

    def decayPheromone(self):
        self.pheromones *= (1-self.rho)
     
    @staticmethod 
    def computePheromoneDelta(cost, feasibility, q, posInf, penalty):
        
        if not feasibility :
            cost += penalty
            
        if cost == 0.0 :
            deltaPhero = posInf 
        else:
            deltaPhero = q/cost
            
        return deltaPhero

  
        
    @staticmethod    
    def generateRandNode(arrWeights, rand):
        weightsSum = 0.0
        for i in range(len(arrWeights)):
            weightsSum += arrWeights[i]
                   
        probSum = 0.0;
        r = rand.rand()*weightsSum
    
        for i in range(len(arrWeights)):
            probSum += arrWeights[i]
            if (r <= probSum):
                return i
            
        return len(arrWeights)-1
    

    def constructPathForAnt(self):
        way = []
        while True:
            options = self.problem.getCurOptions(way)
            if not options:
                break;
            way.append(self.getNextStep(way,options))
        cost = self.problem.getCost(way)
        feasibility = self.problem.isCompleteFeasible(way)
        deltaPheromone = AntSolver.computePheromoneDelta(cost, feasibility, self.Q, self.posInf, self.penalty) 
        return WalkedWay(way, cost, feasibility, deltaPheromone)
    
    def getNextStep(self, way, options):
        if len(options) == 1:
            return options[len(options)-1]
        optionWeight = [0]*len(options)
        for i in range(len(options)): # i = index of nodeID
            optionWeight[i] = self.optionProbability(way[-1], options[i])
        nextId = AntSolver.generateRandNode(optionWeight, self.rand)
        nextStep = options[nextId]
        return nextStep

    
    def optionProbability(self, x, y):
        probability = math.pow(self.pheromones[x, y], self.alpha) * math.pow(self.heuristic[x, y], self.beta)
        return probability
    
    
                        
                
            

