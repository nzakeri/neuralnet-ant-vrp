'''
Created on Sep 25, 2017

@author: negar
'''
import tensorflow as tf
import numpy as np

class SimpleAntBrain(object):
    '''
    classdocs    '''


    def __init__(self, nNodes, alpha, beta, rho, heuristic, initPheromone):
        '''
        Constructor
        '''
        # self.problem = problemIns   
        self.probsize = nNodes    
        self.alpha = alpha
        self.beta = beta
        self.rho = rho
        self.heuristic = heuristic
        self.initPheromone = initPheromone
        self.posInf = 100000.0
        self.penalty = 1000000.0 
        
    def initialize(self,nAnts):
        self.nAnts = nAnts
        #self.probsize = self.problem.getMaxOptionIdx()
        pherShape = [self.probsize, self.probsize]
        curNodeshape = [self.nAnts, self.probsize]
        pheromones = np.full(pherShape, self.initPheromone)        
        
        self.curNodeHolder = tf.placeholder(tf.float32, shape = curNodeshape)
        self.pherVariable = tf.Variable(pheromones,dtype=tf.float32,name='pherVariable')
        self.heuristicVariable = tf.Variable(self.heuristic,
                                             dtype=tf.float32,
                                             name='heuristicVariable'
                                             ) 
        
               
        pher_level = tf.matmul(self.curNodeHolder, self.pherVariable)
        heuristic_desirability = tf.matmul(self.curNodeHolder, self.heuristicVariable)
        pherVal = tf.pow(pher_level, self.alpha)
        heuristicVal = tf.pow(heuristic_desirability, self.beta)
        
        self.prob = tf.multiply(pherVal,heuristicVal)
        
        self.deltaPheroHolder = tf.placeholder(tf.float32, pherShape, name='deltaPheroVariable')
        
        self.updatePhermoneOp = self.pherVariable.assign( self.pherVariable * (1-self.rho) + self.deltaPheroHolder )
        
        init = tf.global_variables_initializer()       
        tf.get_default_session().run(init) 
        
        
        
    def computePheromoneDeltaMat(self, paths):
        pherShape = [self.probsize, self.probsize]
        deltaPheroMat = np.full(pherShape, 0.0, dtype=np.float32)
        for path in paths:
            deltaPheromone = path.deltaPheromone; 
#             print(path.way)
            for i in range(len(path.way)-1):
                a = path.way[i]
                b = path.way[i+1]
                deltaPheroMat[a,b] += deltaPheromone 
#                 print(deltaPheroMat)     
        return deltaPheroMat  
    
    def updatePheromone(self, paths):
        pherDeltaMat = self.computePheromoneDeltaMat(paths)
        tf.get_default_session().run(
                self.updatePhermoneOp,
                {self.deltaPheroHolder:pherDeltaMat},
            ) 
   
        
    def computeProbability(self, ways_array):
        
        curNodeshape = [self.nAnts, self.probsize]
        self.curNode= np.full(curNodeshape,0.0, dtype=np.float32)
        
        for i,way in enumerate(ways_array):
            lastId = way[-1]
            self.curNode[i,lastId] = 1.0
            
        probEval = tf.get_default_session().run(self.prob,{self.curNodeHolder:self.curNode})                
        
#         for i,way in enumerate(ways_array):
#             lastId = way[-1]
#             self.curNode[i,lastId] = 0.0
#         
        return(probEval)
        
        
