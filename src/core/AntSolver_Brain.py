'''
Created on Oct 6, 2017

@author: negar
'''
import numpy as np
import tensorflow as tf
from core.WalkedWay import WalkedWay



class AntSolver_Brain(object):
    '''
    classdocs
    '''


    def __init__(self, problemIns, theBrain, Q, nAnts, nIterations, penalty, seed, 
                 log_location, log_interval, batched_computation):
        '''
        Constructor
        '''
        self.theBrain = theBrain
        self.problem = problemIns
        self.Q = np.float32(Q)
        self.nAnts = nAnts
        self.nIterations = nIterations
        self.rand = np.random.RandomState(seed)
        
        self.posInf = 100000.0
        self.penalty = penalty 
        self.infinity = np.float32(1)   
        self.batched_computation = batched_computation
#         self.smartPherUpdate = SimpleAntBrain(self.nNodes, self.nAnts, self.alpha, self.beta, self.rho, self.heuristic, self.initPheromone)    
        
        self.log_interval = log_interval
        
        if self.batched_computation:
            theBrain.initialize(self.nAnts);
        else:
            theBrain.initialize(1);
        
        if (self.log_interval > 0):
        
            self.writer = tf.summary.FileWriter(log_location)        
            self.cost_tensor = tf.placeholder(tf.float32, [self.nAnts] , "cost_holder") # in ACO is the path's length
            self.feasibility_tensor = tf.placeholder(tf.float32, [self.nAnts],"feas_holder")
            self.delta_pher_tensor = tf.placeholder(tf.float32, [self.nAnts],"delta_pher_holder")
            self.best_delta_pher_tensor = tf.placeholder(tf.float32, [],"best_delta_pher_holder")
            self.best_cost_tensor = tf.placeholder(tf.float32, [],"best_cost_holder")
            
            tf.summary.histogram("cost", self.cost_tensor)
            tf.summary.histogram("feasibility", self.feasibility_tensor)
            tf.summary.histogram("delta_pher", self.delta_pher_tensor)
            
            
            tf.summary.scalar('cost_mean', tf.reduce_mean(self.cost_tensor))
            tf.summary.scalar('feas_mean', tf.reduce_mean(self.feasibility_tensor))
            tf.summary.scalar('delta_pher_mean', tf.reduce_mean(self.delta_pher_tensor))
            tf.summary.scalar('best_delta_pher', self.best_delta_pher_tensor)
            tf.summary.scalar('best_cost', self.best_cost_tensor)
        
            self.all_summ = tf.summary.merge_all()
            
    def run(self):
                
        best = WalkedWay([], 0.0, 0.0, 0.0)
                        
        for i in range(self.nIterations):
            
            if self.batched_computation:
                paths = self.constructBatchedPath()
            else:
                paths = []
                for _ in range(self.nAnts):
                    paths.append(self.constructPathForAnt())
            
#             for p in paths:
#                 print (p)
            
            self.theBrain.updatePheromone(paths) 
            
            for path in paths:
                               
                if path.deltaPheromone > best.deltaPheromone:
                    best = path
            
#             print (best.way)
            
            if (i % self.log_interval == 0 and self.log_interval >= 0):
                
                costs = [p.cost for p in paths]
                feases = [p.feasibility for p in paths]
                delta_pher = [p.deltaPheromone for p in paths]
#                 print (feases)
                
                summ_res = self.all_summ.eval({ self.cost_tensor: costs,
                                                self.feasibility_tensor: feases,
                                                self.delta_pher_tensor: delta_pher,
#                                                 self.pher_tensor: self.pheromones,
                                                self.best_delta_pher_tensor: best.deltaPheromone,
                                                self.best_cost_tensor: best.cost
                                            })
                self.writer.add_summary(summ_res, i)
                self.writer.flush();
                print('.', end='', flush=True)
                
        return best
        

#     def initDistHeuristic(self, locArr):
#         disMat = np.full([len(locArr), len(locArr)], 0.0) 
#         self.heuristic = np.full(disMat.shape, 0.0)  
#         for i in range(len(locArr)):
#             for j in range(len(locArr)):                
#                 xDiff = locArr[i][0] - locArr[j][0];
#                 yDiff = locArr[i][1] - locArr[j][1];
#                               
#                 disMat[i,j] = math.fabs(math.sqrt((xDiff * xDiff) + (yDiff * yDiff)))
#                 if disMat[i,j] == 0.0:
#                     self.heuristic[i,j] = self.infinity
#                 else:                
#                     self.heuristic[i,j] = 1.0 / disMat[i,j]  
    def constructBatchedPath(self):
        ways = [ [] for _ in range (self.nAnts) ]
        
        pathes_remaining = True
        for way in ways:
            options = self.problem.getCurOptions(way)
            assert(len(options) == 1),"first option needs to be single and depot"
            way.append(options[0])
        
        while pathes_remaining:
            pathes_remaining = False
            probs = self.theBrain.computeProbability(ways)
            i = 0
            for way in ways:
                options = self.problem.getCurOptions(way)
                if not options:
                    continue;
                nextOpt = self.getOptionWithProb(options,probs[i])
                way.append(nextOpt)
                i+=1
                pathes_remaining = True    
                
        walkedWays = []
      
        for way in ways:
            walkedWays.append(self.createWalkedWayFromWay(way))
            
        return walkedWays
            
    def constructPathForAnt(self):
        way = []
        while True:
            options = self.problem.getCurOptions(way)
            if not options:
                break;
            way.append(self.getNextStep(way,options))
        
        return self.createWalkedWayFromWay(way)
    
    def createWalkedWayFromWay(self,way):    
        cost = self.problem.getCost(way)
        feasibility = self.problem.isCompleteFeasible(way)
        deltaPheromone = AntSolver_Brain.computePheromoneDelta(cost, feasibility, self.Q, self.posInf, self.penalty) 
        return WalkedWay(way, cost, feasibility, deltaPheromone)

    def getNextStep(self, way, options):
        if (len (way) ) > 0:
            probs = self.theBrain.computeProbability([way])
            retval = self.getOptionWithProb(options, probs[0])
        
        if len(options) == 1:
            retval = options[len(options)-1]
        
        return retval
    
    def getOptionWithProb(self,options,probs):    
        optionWeight = []
        for opt in options: # i = index of nodeID
            optionWeight.append(probs[opt])
        nextId = AntSolver_Brain.generateRandNode(optionWeight, self.rand)
        nextStep = options[nextId]
        return nextStep
    
    @staticmethod    
    def generateRandNode(arrWeights, rand):
        weightsSum = 0.0
        for i in range(len(arrWeights)):
            weightsSum += arrWeights[i]
                   
        probSum = 0.0;
        r = rand.rand()*weightsSum
    
        for i in range(len(arrWeights)):
            probSum += arrWeights[i]
            if (r <= probSum):
                return i
            
        return len(arrWeights)-1

    
    @staticmethod 
    def computePheromoneDelta(cost, feasibility, q, posInf, penalty):
        
        if not feasibility :
            cost += penalty
            
        if cost == 0.0 :
            deltaPhero = posInf 
        else:
            deltaPhero = q/cost
            
        return deltaPhero
    
    
       