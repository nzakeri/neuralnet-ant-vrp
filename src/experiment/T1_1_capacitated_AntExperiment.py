'''
Created on Sep 12, 2017

@author: negar
'''
import os
from core.AntSolver import AntSolver
from core.CVRP import CVRP
import tensorflow as tf

# This experiment, tests a simple ACO against the A_VRP. 
# It will produce a graph of the best found path over the iterations.
# The results will be the average of 100 runs, with the STDDEV included
 
runname = "T1-ACO"
os.makedirs("results", exist_ok=True)    
#   ds = CVRPDataset("datasets/CVRP")  
dsFile = "/home/negar/Dropbox/richvrp/dataSets/CVRP/P-n40-k5.vrp"
with open(dsFile) as f:
    dsLines = f.readlines()
 
problem = CVRP(dsLines)         
alpha = 3.0;
beta = 3.0;
rho = 0.0001;
q = 100.0;
seed = 12345;
nAnts = 50;
intPheromone = 2;
nIterations = 51;
nRuns = 20;
log_location_prefix = "../../tf_log/T1-ACO_"
log_interval = 10

curID = 0
while os.path.exists(log_location_prefix+str(curID)):
    curID += 1

log_location = log_location_prefix+str(curID)

for i in range(nRuns):
    new_graph = tf.Graph()
    with new_graph.as_default():
        sess = tf.Session()
        with sess.as_default():

            solver = AntSolver(problem,
                    alpha,
                    beta,
                    rho,
                    q,
                    nAnts,
                    intPheromone,
                    nIterations,
                    seed+i,
                    log_location,
                    log_interval=log_interval,
                    penalty=1000000
                )
    #         import cProfile
    #         cProfile.run('solver.run()')
    
            solver.run()
                 

