'''
Created on Sep 12, 2017

@author: negar
'''
import os
from core.CVRP import CVRP
import tensorflow as tf
from core.AntSolver_Brain import AntSolver_Brain
from core.SimpleAntBrain import SimpleAntBrain


# This experiment, tests a simple ACO against the A_VRP. 
# It will produce a graph of the best found path over the iterations.
# The results will be the average of 100 runs, with the STDDEV included
 
runname = "T1-ACO"
os.makedirs("results", exist_ok=True)    
#   ds = CVRPDataset("datasets/CVRP")  
dsFile = "/home/negar/Dropbox/richvrp/dataSets/CVRP/P-n40-k5.vrp"
with open(dsFile) as f:
    dsLines = f.readlines()
problem = CVRP(dsLines) 
alpha = 3.0;
beta = 3.0;
rho = 0.1;
q = 100.0;
seed = 12345;
nAnts = 50;
initPheromone = 2.0;
nIterations = 50;
nRuns = 50;
penalty = 1000000.0; 
log_location_prefix = "../../tf_log/T1-2_"

curID = 1
while os.path.exists(log_location_prefix + str(curID) ):
    curID += 1

log_location = log_location_prefix + str(curID) + "/"


for i in range(nRuns):
    new_graph = tf.Graph()
    with new_graph.as_default():
        sess = tf.Session()
        with sess.as_default():
            heuristic =  problem.getHeuristic()
            theBrain = SimpleAntBrain(problem.getMaxOptionIdx(), 
                                      alpha, beta, rho, heuristic, initPheromone
                                      )
            solver = AntSolver_Brain( problemIns = problem,
                                      theBrain = theBrain,
                                       Q = q,
                                       nAnts = nAnts,
                                       nIterations = nIterations,
                                       penalty = penalty,
                                       seed = seed+i,
                                       log_location = log_location+"_%s"%(i),
                                       log_interval = 10,
                                       batched_computation = True,
                )
    #         import cProfile
    #         cProfile.run('solver.run()')
    
            solver.run()
                 
