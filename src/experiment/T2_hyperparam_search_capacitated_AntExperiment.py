'''
Created on Sep 12, 2017

@author: negar
'''
import os
from core.AntSolver import AntSolver
from core.CVRP import CVRP
import tensorflow as tf
import pickle
# This experiment, tests a simple ACO against the A_VRP. 
# It will produce a graph of the best found path over the iterations.
# The results will be the average of 100 runs, with the STDDEV included
 
runname = "T1-ACO"
os.makedirs("results", exist_ok=True)    
#   ds = CVRPDataset("datasets/CVRP")  
dsFile = "/home/negar/Dropbox/richvrp/dataSets/CVRP/A-n32-k5.vrp"
with open(dsFile) as f:
    dsLines = f.readlines()
 
problem = CVRP(dsLines)         
alpha = 1.0;
beta = 1.0;
rho = 0.01;
q = 1.0;
seed = 12345;
nAnts = 100;
intPheromone = 2;
nIterations = 1000;
nRuns = 1;
log_location = "../../tf_log/T2-ACO_"

curID = 0
while os.path.exists(log_location+str(curID)):
    curID += 1

log_location += str(curID)

alphas = [0.5,1.0,2.0,4.0]
betas = [0.5,1.0,2.0,4.0]
rhos = [0.003,0.01,0.03,0.1]



os.makedirs("../../res", exist_ok=True)

resultFilename = "../../res/T2-params_0"
if os.path.exists(resultFilename):
    results =  pickle.load( open( resultFilename, "rb" ) )
else:
    results = {}

for alpha in alphas:
    for beta in betas:
        for rho in rhos:
            for i in range(nRuns):
                key = (alpha,beta,rho,i)
                if key in results:
                    print ("skipping %s"%(str(key)))
                    continue
                sess = tf.Session()
                with sess.as_default():
                    
                    solver = AntSolver(problem,
                            alpha,
                            beta,
                            rho,
                            q,
                            nAnts,
                            intPheromone,
                            nIterations,
                            seed+i,
                            log_location,
                            -1,
                        )
                    res = solver.run()
                    results[key] = res
                    pickle.dump( results, open( resultFilename, "wb" ) )
                    print ("%s, "%(res.deltaPheromone), end="", flush=True )
