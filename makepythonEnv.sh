sudo apt install virtualenv -y
sudo apt install python3.5 -y
virtualenv -p /usr/bin/python3.5 pyenv35
source pyenv35/bin/activate
pip install numpy
pip install tensorflow
pip install jupyter 
pip install matplotlib
